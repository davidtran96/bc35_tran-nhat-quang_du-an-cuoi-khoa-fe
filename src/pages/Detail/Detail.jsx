import Footer from "component/Footer/Footer";
import React, { useEffect } from "react";
import { Outlet, useParams } from "react-router-dom";
import DetailRoom from "template/DetailTemplate/DetailRoom";

const Detail = () => {
  const { id } = useParams();
  useEffect(() => {
    window.scroll(0, 0);
  }, []);

  return (
    <div>
      <DetailRoom paramsId={id} />
      <Footer />
    </div>
  );
};

export default Detail;
