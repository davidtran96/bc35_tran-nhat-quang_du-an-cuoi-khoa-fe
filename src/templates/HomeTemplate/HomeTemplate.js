/* eslint-disable react-hooks/exhaustive-deps */

import Footer from "component/Footer/Footer";
import React, { useState } from "react";
import { Outlet } from "react-router-dom";

export default function HomeTemplate() {
  return (
    <div>
      <div
        style={{
          position: "sticky",
          top: "0",
          zIndex: "30",
          backgroundColor: "white",
        }}
      >
        {/* <NavBar></NavBar>
        <Filter></Filter> */}
      </div>

      <Outlet></Outlet>
      <Footer />
    </div>
  );
}
